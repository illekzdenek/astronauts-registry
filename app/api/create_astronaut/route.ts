

import { client, db } from '@/utils/dbHelper';
import { NextRequest, NextResponse } from 'next/server';

export async function POST(request: NextRequest):Promise<NextResponse> {
    
    try {
        await client.connect()
        const body = await request.json()

        const collection = db.collection('astronauts');

        const insertResult = await collection.insertOne({
            name: body.name,
            surname: body.surname,
            birth: body.birth,
            ability: body.ability
        });

        if (insertResult.acknowledged){
            
            const findResult = await collection.find({}).toArray();

            return NextResponse.json(findResult)
            
        }
    

        
        return NextResponse.json({message: "Error: not found any astronaut with this id."})
    } catch (e) {
        await client.close()
        return NextResponse.json({ error: e })
    } finally {
        
        await client.close()
    }

}


