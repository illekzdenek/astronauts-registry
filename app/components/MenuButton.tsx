"use client"
import {useState, MouseEvent} from 'react';
import {MenuItem, Menu, Button} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { useRouter } from 'next/navigation';
import styles from "./MenuButton.module.css"

export default function MenuButton() {

    const router = useRouter()

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleLinkClick = (link:string) => {
    setAnchorEl(null);
    router.push(link)
  }
  const handleClose = (link:string) => {
    setAnchorEl(null);
  };

  return (
    <div 
    className={`${styles.container}`}>
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        <MenuIcon/>
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={()=>handleLinkClick("/")}>🏠Domů</MenuItem>
        <MenuItem onClick={()=>handleLinkClick("/astronauts")}> 👨‍🚀Astronauti</MenuItem>
      </Menu>
    </div>
  );
}
