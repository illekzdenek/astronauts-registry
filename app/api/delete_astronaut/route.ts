

import { NextRequest, NextResponse } from 'next/server';
import { ObjectId } from 'mongodb';
import { client, db } from '@/utils/dbHelper';

export async function POST(request: NextRequest):Promise<NextResponse> {
    
    try {
        await client.connect()
        const body = await request.json()

        const collection = db.collection('astronauts');


        let id = new ObjectId(body._id)
        
        const deleteResult = await collection.deleteOne({ _id: id });
        if (deleteResult.acknowledged && deleteResult.deletedCount === 1 ){
            
            
            const findResult = await collection.find({}).toArray();

            return NextResponse.json(findResult)
            
        }
    

        
        return NextResponse.json({message: "Error: not found any astronaut with this id."})
    } catch (e) {
        await client.close()
        return NextResponse.json({ error: e })
    } finally {
        
        await client.close()
    }

}


