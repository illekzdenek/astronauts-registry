"use client";

import {
  createContext,
  useContext,
  Dispatch,
  SetStateAction,
  useState,
  ReactNode,
} from "react";

export interface Astronauts extends Array<any> {
  _id: string;
  name: string;
  surname: string;
  birth: string;
  ability: string;
}
[];

export interface ContextProps {
  userId: string;
  setUserId: Dispatch<SetStateAction<string>>;
  data: string;
  setData: Dispatch<SetStateAction<string>>;
  astronauts: Astronauts | [];
  setAstronauts: Dispatch<SetStateAction<Astronauts | []>>;
}

const GlobalContext = createContext<ContextProps>({
  userId: "",
  setUserId: (): string => "",
  data: "",
  setData: (): string => "",
  astronauts: [],
  setAstronauts: (): Astronauts | [] => [],
});

export const GlobalContextProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [userId, setUserId] = useState<string>("");
  const [data, setData] = useState<string>("");
  const [astronauts, setAstronauts] = useState<Astronauts | []>([]);

  return (
    <GlobalContext.Provider
      value={{ userId, setUserId, data, setData, astronauts, setAstronauts }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContext = () => useContext(GlobalContext);