import { MongoClient } from "mongodb";


export async function initAstronautsData(): Promise<any> {

    const uri = `${process.env.MONGODB_URI}`
    const client = new MongoClient(uri);

    const dbName = 'astronauts';
    const db = client.db(dbName);

    try {
        await client.connect()
        const collection = db.collection('astronauts');

        const findResult = await collection.find({}).toArray();

        return findResult
    } catch (e) {
        client.close()
        return { error: e }
    } finally {

        await client.close()
    }
}