
import styles from "./page.module.css";


export default function Home() {


  return (
    <main className={` ${styles.main}`}>
      <h1 className="text-3xl font-bold underline">Aplikace pro správu astronautů!</h1>
      <p>
        Vítejte v naší aplikaci pro správu astronautů! Tato aplikace vám umožní
        snadnou a efektivní správu všech astronautů ve vaší organizaci. Díky
        naší aplikaci budete mít přehled o jejich úkolech, misích, trénincích a
        zdravotním stavu. S naší aplikací budete mít k dispozici všechny
        důležité informace o vašich astronautech na jednom místě a v reálném
        čase. Naše aplikace vám také umožní snadnou komunikaci s vašimi
        astronauty a řešení případných problémů. Jsme si jisti, že naše aplikace
        vám usnadní práci a zlepší vaši organizaci. Vstupte do světa správy
        astronautů s naší aplikací!
      </p>
    </main>
  );
}
