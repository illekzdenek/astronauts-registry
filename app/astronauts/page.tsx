import styles from "./page.module.css";

import CustomTable from "@/app/components/global/CustomTable";
import NewAstronaut from "./components/NewAstronaut";
import Refresh from "./components/Refresh";

import { initAstronautsData } from "@/utils/initHelper";
import { Astronauts } from "../Context/store";

interface PageProps {
  params: any;
  searchParams: any;
}

export default async function AstronautsPage(props: PageProps) {
  const data = await initAstronautsData();
  let parsedData = [];
  for (let d of data) {
    parsedData.push({
      ...d,
      _id: d._id.toString(),
    });
  }
  return (
    <main className={`${styles.container}`}>
      <div className={`${styles.row}`}>
        <h1>Správa astronautů</h1>
      </div>

      <div className={`${styles.row}`}>
        <p>
          Zde můžete spravovat astronauty. Pomocí tlačítka můžete přidat nové
          astronauty. Již existující astronauty je možné spravovat v tabulce. Je
          možné je upravit, nebo smazat ve sloupci akcí v tabulce. Tabulka
          obsahuje stránkování a zároveň umožňuje seřadit astronauty podle
          jednotlivých vlastností.
        </p>
      </div>

      <div className={`${styles.row} ${styles.table__actions}`}>
        <NewAstronaut />
        <Refresh />
      </div>
      <div className={`${styles.row}`}>
        <CustomTable options={parsedData as Astronauts} />
      </div>
    </main>
  );
}
