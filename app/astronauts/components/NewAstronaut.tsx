"use client";

import { Astronauts, useGlobalContext } from "@/app/Context/store";

import {
  CircularProgress,
  DialogTitle,
  DialogContentText,
  DialogContent,
  Dialog,
  TextField,
  Button,
  DialogActions,
} from "@mui/material";
import { ChangeEvent, ReactElement, useState } from "react";

interface NewAstronautProps {}

export default function NewAstronaut(props: NewAstronautProps) {
  const { setAstronauts } = useGlobalContext();

  const [open, setOpen] = useState<boolean>(false);
  const [name, setName] = useState<string>("");
  const [surname, setSurname] = useState<string>("");
  const [birth, setBirth] = useState<string>("");
  const [ability, setAbility] = useState<string>("");
  const [waiting, setWaiting] = useState<boolean>(false);

  const handleClickOpen = (): void => {
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  const handleAdd = async (): Promise<void> => {
    setWaiting(true);

    const res = await fetch(`/api/create_astronaut`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        surname,
        birth,
        ability,
      }),
    });
    const data = await res.json();

    if (Array.isArray(data)) {
      setAstronauts(data as Astronauts);
    }

    setWaiting(false);
    setOpen(false);
  };

  const handleNameChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setName(element.value);
  };

  const handleSurnameChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setSurname(element.value);
  };

  const handleBirthChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setBirth(element.value);
  };

  const handleAbilityChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setAbility(element.value);
  };

  const getCreateButton = (): ReactElement => {
    if (waiting) {
      return (
        <Button disabled>
          <CircularProgress />
        </Button>
      );
    }
    return <Button onClick={handleAdd}>Vytvořit</Button>;
  };

  return (
    <div>
      <Button className="button" variant="contained" onClick={handleClickOpen}>
        Přidat astronauta
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Nový astronaut</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Pro přidání astronauta vyplňte všechna pole a potvrďte stisknutím
            tlačítka.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Jméno"
            type="text"
            fullWidth
            variant="standard"
            value={name}
            onChange={handleNameChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="surname"
            label="Přijmení"
            type="text"
            fullWidth
            variant="standard"
            value={surname}
            onChange={handleSurnameChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="birth"
            label="Datum narození"
            type="date"
            fullWidth
            variant="standard"
            value={birth}
            onChange={handleBirthChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            autoFocus
            margin="dense"
            id="ability"
            label="Speciální schopnost"
            type="text"
            fullWidth
            variant="standard"
            value={ability}
            onChange={handleAbilityChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Zpět</Button>
          {getCreateButton()}
        </DialogActions>
      </Dialog>
    </div>
  );
}
