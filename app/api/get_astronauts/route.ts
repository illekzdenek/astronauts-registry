
import { NextResponse } from 'next/server';
import { client, db } from '@/utils/dbHelper';


export async function GET():Promise<NextResponse> {

    try {
        await client.connect()
        
        const collection = db.collection('astronauts');

        const findResult = await collection.find({}).toArray();

        return NextResponse.json(findResult)
    } catch (e) {
        await client.close()
        return NextResponse.json({ error: e })
    } finally {
        
        await client.close()
    }

}


