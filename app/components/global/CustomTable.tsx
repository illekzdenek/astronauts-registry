"use client";
import {
  ReactElement,
  SyntheticEvent,
  useEffect,
  useState,
  MouseEvent,
  ChangeEvent,
} from "react";
import { useTheme } from "@mui/material/styles";

import styles from "./CustomTable.module.css";
import { useGlobalContext, Astronauts } from "@/app/Context/store";

import {
  Button,
  IconButton,
  Input,
  TableFooter,
  TablePagination,
  Paper,
  TableRow,
  TableHead,
  TableContainer,
  TableBody,
  Table,
  CircularProgress,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import TableCell, {
  tableCellClasses,
  TableCellProps,
} from "@mui/material/TableCell";
import Box from "@mui/system/Box/Box";

import FirstPageIcon from "@mui/icons-material/FirstPage";
import LastPageIcon from "@mui/icons-material/LastPage";
import {
  ArrowDropDown,
  ArrowDropUp,
  KeyboardArrowLeft,
  KeyboardArrowRight,
} from "@mui/icons-material";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

interface CustomTableProps {
  options: Astronauts;
}

const headings: {
  value: string;
  title: string;
  align: TableCellProps["align"];
}[] = [
  {
    value: "name",
    title: "Jméno",
    align: "left",
  },
  {
    value: "surname",
    title: "Přijmení",
    align: "left",
  },
  {
    value: "birth",
    title: "Narození",
    align: "left",
  },
  {
    value: "ability",
    title: "Super schopnost",
    align: "left",
  },
  {
    value: "_id",
    title: "Identifikátor",
    align: "right",
  },
  {
    value: "action",
    title: "Akce",
    align: "center",
  },
];

export default function CustomTable(props: CustomTableProps) {
  const { astronauts, setAstronauts } = useGlobalContext();

  const [editModeTo, setEditModeTo] = useState<string>("");
  const [waitingTo, setWaitingTo] = useState<string>("");
  const [waitingForDelete, setWaitingForDelete] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [surname, setSurname] = useState<string>("");
  const [birth, setBirth] = useState<string>("");
  const [ability, setAbility] = useState<string>("");
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);

  useEffect(() => {
    setAstronauts(props.options);
  }, []);

  const handleChangePage = (e: SyntheticEvent | null, b: number): void => {
    setPage(b);
  };

  const handleChangeRowsPerPage = (e: SyntheticEvent): void => {
    const element = e.target as HTMLInputElement;
    setRowsPerPage(Number(element.value));
  };

  const handleFilterSet = (filterBy: string, sortType: string): void => {
    let sortedAstronauts = [...astronauts];
    if (sortType === "asc") {
      sortedAstronauts.sort((a, b) => a[filterBy].localeCompare(b[filterBy]));
    }
    if (sortType === "desc") {
      sortedAstronauts.sort((a, b) => -a[filterBy].localeCompare(b[filterBy]));
    }
    setAstronauts(sortedAstronauts as Astronauts);
  };

  const handleRowDelete = async (id: string): Promise<void> => {
    setWaitingForDelete(id);

    const res = await fetch(`/api/delete_astronaut`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        _id: id,
      }),
    });
    const data = await res.json();

    if (Array.isArray(data)) {
      setAstronauts(data as Astronauts);
    }
    setWaitingForDelete("");
  };
  const handleRowEdit = (e: MouseEvent, id: string): void => {
    if (id === editModeTo) {
      setEditModeTo("");
    } else {
      setEditModeTo(id);
      let astronaut = astronauts.find((astr) => astr._id === id);
      setName(astronaut ? astronaut.name : "");
      setSurname(astronaut ? astronaut.surname : "");
      setBirth(astronaut ? astronaut.birth : "");
      setAbility(astronaut ? astronaut.ability : "");
    }
  };

  const handleWriteChanges = async (
    e: MouseEvent,
    id: string
  ): Promise<void> => {
    setWaitingTo(id);
    setEditModeTo("");

    const res = await fetch(`/api/change_astronaut`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        surname,
        birth,
        ability,
        _id: editModeTo,
      }),
    });
    const data = await res.json();

    if (Array.isArray(data)) {
      setAstronauts(data as Astronauts);
    }

    setWaitingTo("");
  };
  const handleSurnameChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setSurname(element.value);
  };
  const handleNameChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setName(element.value);
  };

  const handleBirthChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;
    setBirth(element.value);
  };

  const handleAbilityChange = (e: ChangeEvent): void => {
    const element = e.target as HTMLInputElement;

    setAbility(element.value);
  };

  const getHeaderCells = (): ReactElement[] => {
    const headerElements = [];
    for (let heading of headings) {
      if (["name", "surname", "birth", "ability"].includes(heading.value)) {
        headerElements.push(
          <StyledTableCell align={heading.align} key={heading.value}>
            <div className="flex items-center">
              <div>
                <div onClick={() => handleFilterSet(heading.value, "asc")}>
                  <ArrowDropUp />
                </div>
                <div onClick={() => handleFilterSet(heading.value, "desc")}>
                  <ArrowDropDown />
                </div>
              </div>
              {heading.title}
            </div>
          </StyledTableCell>
        );
      } else {
        headerElements.push(
          <StyledTableCell align={heading.align} key={heading.value}>
            {heading.title}
          </StyledTableCell>
        );
      }
    }

    return headerElements;
  };

  const getContentRows = (): ReactElement => {
    if (Array.isArray(astronauts)) {
      const rowsToDisplay =
        rowsPerPage > 0
          ? astronauts.slice(
              page * rowsPerPage,
              page * rowsPerPage + rowsPerPage
            )
          : astronauts;

      const rows = rowsToDisplay.map((row) => {
        if (editModeTo === row._id) {
          return (
            <StyledTableRow key={row._id}>
              <StyledTableCell component="th" scope="row">
                <Input value={name} onChange={handleNameChange} />
              </StyledTableCell>
              <StyledTableCell align="right">
                <Input value={surname} onChange={handleSurnameChange} />
              </StyledTableCell>
              <StyledTableCell align="right">
                <Input value={birth} type="date" onChange={handleBirthChange} />
              </StyledTableCell>
              <StyledTableCell align="right">
                <Input value={ability} onChange={handleAbilityChange} />
              </StyledTableCell>
              <StyledTableCell align="right">{row._id}</StyledTableCell>
              <StyledTableCell align="right">
                {getActionsCell(row._id)}
              </StyledTableCell>
            </StyledTableRow>
          );
        }
        return (
          <StyledTableRow key={row._id}>
            <StyledTableCell component="th" scope="row">
              {row.name}
            </StyledTableCell>
            <StyledTableCell align="right">{row.surname}</StyledTableCell>
            <StyledTableCell align="right">
              {new Date(row.birth).toLocaleDateString()}
            </StyledTableCell>
            <StyledTableCell align="right">{row.ability}</StyledTableCell>
            <StyledTableCell align="right">{row._id}</StyledTableCell>
            <StyledTableCell align="right">
              {getActionsCell(row._id)}
            </StyledTableCell>
          </StyledTableRow>
        );
      });

      return <>{rows}</>;
    }

    return (
      <StyledTableRow>
        <StyledTableCell component="th" scope="row">
          DB Error - Zkuste stránku načíst znovu nebo kliknout na tlačítko
          &quot;obnovit&quot;.
        </StyledTableCell>

        <StyledTableCell align="right"></StyledTableCell>
        <StyledTableCell align="right"></StyledTableCell>
        <StyledTableCell align="right"></StyledTableCell>
        <StyledTableCell align="right"></StyledTableCell>
        <StyledTableCell align="right"></StyledTableCell>
      </StyledTableRow>
    );
  };

  const getEditButton = (id: string): ReactElement => {
    if (id === waitingTo) {
      return (
        <div>
          <CircularProgress />
        </div>
      );
    }
    if (id === editModeTo) {
      return (
        <div className="flex flex-col">
          <Button onClick={(e) => handleRowEdit(e, id)}>❌</Button>
          <Button onClick={(e) => handleWriteChanges(e, id)}>✅</Button>
        </div>
      );
    }

    return <Button onClick={(e) => handleRowEdit(e, id)}>Upravit</Button>;
  };

  const getDeleteButton = (id: string): ReactElement => {
    if (waitingForDelete === id) {
      return (
        <Button disabled>
          <CircularProgress />
        </Button>
      );
    }

    return <Button onClick={() => handleRowDelete(id)}>Odstranit</Button>;
  };

  const getActionsCell = (id: string): ReactElement => {
    const buttons = [];
    buttons.push(
      <div className="flex justify-end">
        {getDeleteButton(id)}
        {getEditButton(id)}
      </div>
    );

    return <>{buttons}</>;
  };

  return (
    <div className={`${styles.container}`}>
      <Paper>
        <TableContainer className={`${styles.table__container}`}>
          <Table
            stickyHeader
            sx={{ minWidth: 700 }}
            aria-label="customized table"
            className={`${styles.table}`}
          >
            <TableHead>
              <TableRow>{getHeaderCells()}</TableRow>
            </TableHead>
            <TableBody>{getContentRows()}</TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, { label: "Všechny", value: -1 }]}
          colSpan={6}
          component="div"
          count={astronauts.length}
          rowsPerPage={rowsPerPage}
          page={page}
          labelRowsPerPage="Řádků na stránku: "
          SelectProps={{
            inputProps: {
              "aria-label": "rows per page",
            },
            native: true,
          }}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </Paper>
    </div>
  );
}

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (event: MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: MouseEvent<HTMLButtonElement>
  ): void => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: MouseEvent<HTMLButtonElement>
  ): void => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: MouseEvent<HTMLButtonElement>
  ): void => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: MouseEvent<HTMLButtonElement>
  ): void => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}
