"use client";

import { Astronauts, useGlobalContext } from "@/app/Context/store";
import { Button, CircularProgress } from "@mui/material";
import { ReactElement, useState } from "react";

type Props = {};

export default function Refresh({}: Props) {
  const { setAstronauts } = useGlobalContext();

  const [waiting, setWaiting] = useState<boolean>(false);

  const handleRefreshButtonClick = async (): Promise<void> => {
    setWaiting(true);

    const res = await fetch(`/api/get_astronauts`);
    const data = await res.json();

    if (Array.isArray(data)) {
      setAstronauts(data as Astronauts);
    }

    setWaiting(false);
  };

  const getRefreshButton = (): ReactElement => {
    if (waiting) {
      return (
        <Button disabled>
          <CircularProgress />
        </Button>
      );
    }

    return <Button className="button" variant="contained" onClick={handleRefreshButtonClick}>Obnovit</Button>;
  };

  return <div>{getRefreshButton()}</div>;
}
