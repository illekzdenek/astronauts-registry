import styles from "./Header.module.css";
import MenuButton from "./MenuButton";

export default function Header() {
  return (
    <nav className={styles.container}>
      <div className={styles.logo}>Astronautio </div>
      <MenuButton />
    </nav>
  );
}
