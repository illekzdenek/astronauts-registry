

import { NextRequest, NextResponse } from 'next/server';
import { ObjectId } from 'mongodb';
import { client, db } from '@/utils/dbHelper';


export async function POST(request: NextRequest):Promise<NextResponse> {
    
    try {

        await client.connect()
        const body = await request.json()
        
        const collection = db.collection('astronauts');

        let id = new ObjectId(body._id)

        
        const updateResult = await collection.updateOne({ _id: id }, { $set: { 
            name: body.name,
            surname: body.surname,
            birth: body.birth,
            ability: body.ability
        } });

        if (updateResult.acknowledged && updateResult.modifiedCount === 1 ){
            
            
            const findResult = await collection.find({}).toArray();

            return NextResponse.json(findResult)
            
        }
    

        
        return NextResponse.json({message: "Error: not found any astronaut with this id."})
    } catch (e) {
        await client.close()
        return NextResponse.json({ error: e })
    } finally {
        
        await client.close()
    }

}


