import { MongoClient } from "mongodb";



export const uri = `${process.env.MONGODB_URI}`
export const client = new MongoClient(uri);

export const dbName = 'astronauts';
export const db = client.db(dbName);